﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class AltarPuzzleClue : MonoBehaviour
{
    public Canvas EPromptOverlay;
    public Canvas AltarClueUi;
    public string Bookcontent;
    public Text Booktext;

    void OnTriggerStay()
    {
        //Checking if the chest is not open, then if the player presses 'E'. this will bring up the chest code UI. 
        if (Input.GetKey(KeyCode.E))
        {

            AltarClueUi.enabled = true;
            Booktext.text = Bookcontent;
            EPromptOverlay.enabled = false;
        }
        //Checking if the player presses 'Esc'. This will disable the chest puzzle UI and bring back the interaction ui.
        else if (Input.GetKey(KeyCode.Escape))
        {
            AltarClueUi.enabled = false;
            EPromptOverlay.enabled = true;
            //lock the mouse
        }
    }
    void OnTriggerEnter(Collider ObjectThatWalksIn)
    {
            if (ObjectThatWalksIn.tag == "Player")
            {
                EPromptOverlay.enabled = true;
            }
    }
    void OnTriggerExit(Collider ObjectThatWalksIn)
    {
        if (ObjectThatWalksIn.tag == "Player")
        {
            EPromptOverlay.enabled = false;
            AltarClueUi.enabled = false;
        }
    }
}
    


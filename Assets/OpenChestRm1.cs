﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class OpenChestRm1 : MonoBehaviour
{
    //Setting up variables 
    //The interaction canvas variable
    public Canvas EPromptOverlay;
    //The Ui canvas for the chest puzzle 
    public Canvas ChestCodeOverlay;
    //Weither the chest is open or not
    public bool ChestOpen = false;
    //The chest animation
    public Animation Chestanim;
    //The chest code (for puzzle)
    public string ChestCode = "1658";
    //The input feild text (Chest puzzle UI)
    public Text ChestCodeInputFeild;
    public bool HiddenCanvas = false;


    void Update()
    {
        //Checking if the chest is not open and if the code inputed into the ui is correct. If it is, then the chest animation plays and the bool 'ChestOpen' is set to true. it also gets rid of the chest code ui. 
        if(ChestOpen == false)
        {
          //  Debug.Log("Bool = false");
            if (ChestCodeInputFeild.text == ChestCode)
            {
         //       Debug.Log("Chest part");
                Chestanim.Play();
                ChestOpen = true;
                //ChestCodeOverlay.enabled = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
     //   else if(ChestOpen == true && HiddenCanvas == false)
    //    {
      //      Debug.Log("Else part");
//
       //     ChestCodeOverlay.enabled = false;
       //     HiddenCanvas = true;
       // }

    }

    //If the player stays within the trigger box, this code occurs.
    void OnTriggerStay()
    {
        //Checking if the chest is not open, then if the player presses 'E'. this will bring up the chest code UI. 
        if (Input.GetKey(KeyCode.E) && ChestOpen == false)
        {
            Cursor.lockState = CursorLockMode.None;
            ChestCodeOverlay.enabled = true;
            EPromptOverlay.enabled = false;
        }
        //Checking if the player presses 'Esc'. This will disable the chest puzzle UI and bring back the interaction ui.
        else if (Input.GetKey(KeyCode.Escape))
        {
            ChestCodeOverlay.enabled = false;
            EPromptOverlay.enabled = true;
            //lock the mouse
        }
    }

    //When the player enters the trigger area, this code occurs. 
    void OnTriggerEnter(Collider ObjectThatWalksIn)
    {
        //Checks if the chest is open. If it isnt, the interaction ui appears. 
        if (ObjectThatWalksIn.tag == "Player" && ChestOpen == false)
        {
            EPromptOverlay.enabled = true;
        }
    }

    //When the player leaves the trigger area, this code occurs. 
    void OnTriggerExit(Collider ObjectThatWalksIn)
    {
        //Disables all UI. 
        if (ObjectThatWalksIn.tag == "Player")
        {
            EPromptOverlay.enabled = false;
            ChestCodeOverlay.enabled = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}

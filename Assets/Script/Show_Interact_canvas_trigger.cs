﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Gives us access to unit's built in ui stuff
using UnityEngine.UI;

public class Show_Interact_canvas_trigger : MonoBehaviour
{
    public Canvas OverlayScreen;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider ObjectThatWalksIn)
    {
        if(ObjectThatWalksIn.tag == "Player")
        {
            OverlayScreen.enabled = true;
        }
    }
    void OnTriggerExit(Collider ObjectThatWalksIn)
    {
        if (ObjectThatWalksIn.tag == "Player")
        {
            OverlayScreen.enabled = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class OpenDoor : MonoBehaviour
{
    //Setting variables
    //Setting the interaction prompt canvas up
    public Canvas OverlayScreen;
    //selecting animation
    public Animation Dooranimation;
    //checking if the door is open or not
    public bool jaildooropen = false;
    //Does the player have the key for this door?
    public bool HaveKey;
    public string UserText;
    public Text UserSpeach;
    public Canvas UserSpeach_canvas;

    void Update()
    {
        if (HaveKey == false)
        {
            UserText = "Its Locked. I need a key";
        }

    }

    //If the player stays within the trigger box, this code occurs.
    void OnTriggerStay()
    {
        //Checking if the player has pressed 'E' and the door is not opened. If this is true, the door plays its animation and the variable 'jaildooropen' is true
        if (Input.GetKey(KeyCode.E) && jaildooropen == false && HaveKey == true)
        {
            Debug.Log("Open Door");
            // Dooranimation.SetBool("OpenDoor", true);
            Dooranimation.Play();
            jaildooropen = true;
        }
        else if (Input.GetKey(KeyCode.E) && jaildooropen == false && HaveKey == false)
        {
            Debug.Log("Locked door");
            UserSpeach_canvas.enabled = true;
            UserSpeach.text = UserText;
            OverlayScreen.enabled = false;
        }
        else if (Input.GetKey(KeyCode.Escape))
        {
            UserSpeach_canvas.enabled = false;
            OverlayScreen.enabled = true;
            //lock the mouse
        }


    }

    //When the player enters the trigger area, this code occurs. 
    void OnTriggerEnter(Collider ObjectThatWalksIn)
    {
        //if the door is closed, triggers the interaction ui
        if (ObjectThatWalksIn.tag == "Player" && jaildooropen == false)
        {
            Debug.Log("Enter area");
            OverlayScreen.enabled = true;
        }
    }

    //When the player leaves the trigger area, this code occurs.
    void OnTriggerExit(Collider ObjectThatWalksIn)
    {
        //Disables all Ui
        if (ObjectThatWalksIn.tag == "Player")
        {
            Debug.Log("Left area");
            OverlayScreen.enabled = false;
            UserSpeach_canvas.enabled = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

    }
}


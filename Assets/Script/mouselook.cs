﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouselook : MonoBehaviour
{
    //Variables
    //setting the valse for the mouse sensivitity
    public float MouseSens = 100f;

    //Idenifying the players body (cylinder)
    public Transform playerbody;
    float xrotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //locks the mouse in place
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float MouseX = Input.GetAxis("Mouse X") * MouseSens * Time.deltaTime;
        float MouseY = Input.GetAxis("Mouse Y") * MouseSens * Time.deltaTime;

        xrotation -= MouseY;
        xrotation = Mathf.Clamp(xrotation,-90f, 90f);

        transform.localRotation = Quaternion.Euler(xrotation,0f,0f);
        playerbody.Rotate(Vector3.up * MouseX);
    }
}
